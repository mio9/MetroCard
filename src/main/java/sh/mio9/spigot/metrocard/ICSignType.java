package sh.mio9.spigot.metrocard;

import java.util.HashMap;
import java.util.Map;

public enum ICSignType {
    // note to me: Changing these will break existing signs in servers, proceed with caution
    PAY("§3[mpay]"),
    CHARGE("§3[mcharge]"),
    NEW("§3[missue]"),

    STATION_IN("§3[station_in]"),
    STATION_OUT("§3[station_out]");

    private final String signHeader;
    private static final Map<String, ICSignType> lookup = new HashMap<>();

    static {
        for (ICSignType t : ICSignType.values()) {
            lookup.put(t.signHeader, t);
        }
    }

    private ICSignType(String signHeader) {
        this.signHeader = signHeader;
    }

    public String getSignHeader() {
        return this.signHeader;
    }

    public static ICSignType get(String signHeader) {
        return lookup.get(signHeader);
    }
}

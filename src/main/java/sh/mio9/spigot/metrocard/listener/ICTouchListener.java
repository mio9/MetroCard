package sh.mio9.spigot.metrocard.listener;

import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.SoundCategory;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import sh.mio9.spigot.metrocard.MetroCard;
import sh.mio9.spigot.metrocard.events.ICReaderInteractEvent;
import sh.mio9.spigot.metrocard.util.ColorParser;
import sh.mio9.spigot.metrocard.util.HandyUtils;

import java.util.List;

public class ICTouchListener implements Listener {

    @EventHandler
    public void onTouch(ICReaderInteractEvent e) {
        switch (e.getSignType()) {
            case NEW:
                issueNew(e);
                break;
            case PAY:
                pay(e);
                break;
            case CHARGE:
                charge(e);
                break;
            // STATION_IN and STATION_OUT is listened by listeners in PixRail Core
            default:
                return;
            // normally won't have any other cases, just put the default in case some shit went wrong
        }
        // Code goes Here
    }


    private void pay(ICReaderInteractEvent e) {
        /*
       [mpay]
       40
       mio9
         */
        if (!e.getCard().isActive()) {
            playFailSound(e.getSign());
            e.getPlayer().sendMessage(ColorParser.p("&cPayment Failed\n---------------\n" +
                    "&c[!] &4Your IC card is deactivated!" + "\n" +
                    "&eCard Balance: " + String.format("%.2f", e.getCard().getBalance())
            ));
            return;
        }

        if (e.getSign().getLine(1).isEmpty() || e.getSign().getLine(2).isEmpty()) {
            // The sign is incomplete , line 2 and 3 missing
            playFailSound(e.getSign());
            e.getPlayer().sendMessage(ColorParser.p("&cThe payment sign lacks information to work, notify sign creator for this issue"));
            return;
        }
        if (!HandyUtils.isNumeric(e.getSign().getLine(1))) {
            // Line 2 is not a number (price is not number)
            playFailSound(e.getSign());
            e.getPlayer().sendMessage(ColorParser.p("&cThe payment sign pays non numeric value"));
            return;
        }
        double paymentAmount = Double.parseDouble(e.getSign().getLine(1));
        OfflinePlayer receivePlayer = Bukkit.getOfflinePlayer(e.getSign().getLine(2));
        if (receivePlayer == null) {
            playFailSound(e.getSign());
            e.getPlayer().sendMessage(ColorParser.p("&cThis sign does not pay anyone!"));
        } else {
            boolean cardResponse = e.getCard().deduct(paymentAmount);
            EconomyResponse r = MetroCard.plugin.getEcon().depositPlayer(receivePlayer, paymentAmount);
            if (!cardResponse || !r.transactionSuccess()) {
                playFailSound(e.getSign());
                e.getPlayer().sendMessage(ColorParser.p("&cPayment Failed\n---------------\n" +
                        "&7[!] Minimum balance for IC Cards are " + MetroCard.plugin.getConfig().getDouble("min-balance") + "\n" +
                        "&eTo-Pay: " + paymentAmount + "\n" +
                        "&eCard Balance: " + String.format("%.2f", e.getCard().getBalance())
                ));
            } else {
                playSuccessSound(e.getSign());
                e.getPlayer().sendMessage(ColorParser.p("&aTransaction Success\n---------------\n" +
                        "&ePaid: " + paymentAmount + "\n" +
                        "&eCard Balance: " + String.format("%.2f", e.getCard().getBalance())
                ));
            }
        }

    }

    private void charge(ICReaderInteractEvent e) {
        /*
        [mcharge]

         */
        if (!e.getCard().isActive()) {
//            playFailSound(e.getSign());
            e.getPlayer().sendMessage(ColorParser.p("&cPayment Failed\n---------------\n" +
                    "&c[!] &4Your IC card is deactivated!" + "\n" +
                    "&eCard Balance: " + String.format("%.2f", e.getCard().getBalance())
            ));
            return;
        }
    }

    private void issueNew(ICReaderInteractEvent e) {
        /*
        [missue]
         */

        // Opens up GUI
    }

    private void playSuccessSound(Sign s) {
        FileConfiguration config = MetroCard.plugin.getConfig();
        s.getWorld().playSound(s.getLocation(),
                config.getString("sound-success.sound"),
                SoundCategory.valueOf(config.getString("sound-success.category")),
                (float) config.getDouble("sound-success.volume"),
                (float) config.getDouble("sound-success.pitch"));

    }

    private void playFailSound(Sign s) {
        {
            FileConfiguration config = MetroCard.plugin.getConfig();
            s.getWorld().playSound(s.getLocation(),
                    config.getString("sound-fail.sound"),
                    SoundCategory.valueOf(config.getString("sound-fail.category")),
                    (float) config.getDouble("sound-fail.volume"),
                    (float) config.getDouble("sound-fail.pitch"));
        }
    }

    private void runActions(Sign s) {
        String x = String.valueOf(s.getLocation().getBlockX());
        String y = String.valueOf(s.getLocation().getBlockY());
        String z = String.valueOf(s.getLocation().getBlockZ());
        List<String> actionStringList = MetroCard.plugin.getData().getStringList("cards."+x+"/"+y+"/"+z+".actions");


        //todo fuck me i forgot what to do

    }
}

package sh.mio9.spigot.metrocard.listener.vanilla;

import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import sh.mio9.spigot.metrocard.ICCard;
import sh.mio9.spigot.metrocard.ICCardManager;
import sh.mio9.spigot.metrocard.ICSignType;
import sh.mio9.spigot.metrocard.events.ICReaderInteractEvent;
import sh.mio9.spigot.metrocard.util.ColorParser;

public class SignClickChangeListener implements Listener {

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        if (!event.getPlayer().hasPermission("metrocard.sign.create")) {
            return;
        }
        String signHead = event.getLine(0);
//        System.out.println(signHead);
        ICSignType type = ICSignType.get("§3" + signHead);
        if (type == null) {
            return;
        }
        event.setLine(0, "§3" + signHead);
        event.getPlayer().sendMessage(ColorParser.p("&aMetroCard sign created!"));
    }

    @EventHandler
    public void onSignClick(PlayerInteractEvent event) {
        if (!event.hasBlock() || !event.hasItem()) {
//            System.out.println("no block no item");
            return;
        }
//        System.out.println(event.getClickedBlock().getType()+" "+event.getItem().getType());
        if (event.getClickedBlock().getState() instanceof Sign) {
            Sign sign = (Sign) event.getClickedBlock().getState();
            // removed the stripcolor on purpose to avoid forged signs
            ICSignType type = ICSignType.get(sign.getLine(0));
//            System.out.println(type);
            if (type == null) {
                // not a metrocard sign
                return;
            }
            // assume the sign is the metrocard sign
            ICCard card = ICCardManager.getCard(event.getItem());
            if (card != null) {
                // when touched with a card
                ICReaderInteractEvent interactEvent = new ICReaderInteractEvent(card, type, sign, event.getPlayer());

                // fire the event
                Bukkit.getPluginManager().callEvent(interactEvent);


            } else {
                // just randomly poking the sign
                return;
            }
        }

    }
}

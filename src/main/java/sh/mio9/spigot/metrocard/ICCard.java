package sh.mio9.spigot.metrocard;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import sh.mio9.spigot.metrocard.util.ColorParser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.CRC32;

//Todo emit transaction events on every single thing here
// todo emit a

/**
 * Represents the IC Card itself
 */
public class ICCard implements Serializable {
    private String cardID;
    private double balance;
    private boolean active;

    /**
     * Create a default anonymous card with default initial balance
     */
    public ICCard() {
        this.cardID = generateNewID();
        this.balance = Float.valueOf(MetroCard.plugin.getConfig().getString("initial-balance"));
        this.active = true;
    }

    /**
     * Create card with initial balance
     *
     * @param balance Initial Balance
     */
    public ICCard(double balance) {
        this.cardID = generateNewID();
        this.balance = balance;
        this.active = true;
    }


    /**
     * Internal use
     */
    public ICCard(String cardID, double balance, boolean active) {
        this.cardID = cardID;
        this.balance = balance;
        this.active = active;
    }

    /**
     * Charge (top-up) the IC card
     *
     * @param amount Amount to charge
     * @return TRUE when charging is successful, else FALSE
     */
    public boolean charge(double amount) {
        if (this.balance + amount > MetroCard.plugin.getConfig().getDouble("max-balance")) {
            return false;
        } else {
            this.balance += amount;
            update();
            return true;
        }
    }

    /**
     * Deduct an amount of the IC Card
     *
     * @param amount Amount to deduct
     * @return TRUE if it success, else FALSE
     */
    public boolean deduct(double amount) {
        if (this.balance - amount < MetroCard.plugin.getConfig().getDouble("min-balance")) {
            return false;
        } else {
            this.balance -= amount;
            update();
            return true;
        }
    }


    public String getCardID() {
        return cardID;
    }

    public double getBalance() {
        return balance;
    }

    public boolean setBalance(double newBalance) {
        if (newBalance >= MetroCard.plugin.getConfig().getDouble("min-balance") && newBalance <= MetroCard.plugin.getConfig().getDouble("max-balance")) {
            this.balance = newBalance;
            update();
            return true;
        } else {
            return false;
        }

    }

    public void resetBalance() {
        this.balance = MetroCard.plugin.getConfig().getDouble("initial-balance");
        update();
    }

    public ItemStack getItem() {
        String itemType = MetroCard.plugin.getConfig().getString("card-item");
        String itemName = MetroCard.plugin.getConfig().getString("card-name");
        ArrayList<String> lore = new ArrayList<>();

        ItemStack cardItem = new ItemStack(Material.getMaterial(itemType));
        ItemMeta meta = cardItem.getItemMeta();

        lore.add("");
        lore.add(ColorParser.p("&8" + this.cardID.toUpperCase()));
        meta.setDisplayName(ColorParser.p(itemName));
        meta.setLore(lore);
        cardItem.setItemMeta(meta);
        return cardItem;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active){
        this.active=active;
        update();
    }


    //todo Current date -> int -> crc32 -> string
    private String generateNewID() {
        Date date = new Date();
        CRC32 crc = new CRC32();
        crc.update(String.valueOf(date.getTime()).getBytes());

        return Long.toHexString(crc.getValue());

    }

    private void update() {
        ICCardManager.updateCardData(this);
        MetroCard.plugin.saveData();
    }
}
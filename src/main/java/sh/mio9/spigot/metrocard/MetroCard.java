package sh.mio9.spigot.metrocard;

import co.aikar.commands.PaperCommandManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.Website;
import org.bukkit.plugin.java.annotation.plugin.author.Author;
import sh.mio9.spigot.metrocard.commands.Commands;
import sh.mio9.spigot.metrocard.listener.vanilla.SignClickChangeListener;
import sh.mio9.spigot.metrocard.listener.ICTouchListener;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Logger;

@Plugin(name = "MetroCard", version = "1.0-SNAPSHOT")
@Description("Smooth, Fast and east payment with a smart cards")
@Author("MIO9")
@Website("mio9.sh")

/*

  MetroCard
  [MIO9]

  Credits to ItzBenjyHere for his skript idea of contactless payment cards

 */
public class MetroCard extends JavaPlugin {

    //    private FileConfiguration config;
    private File dataFile;
    private FileConfiguration data;
    private Logger logger = this.getLogger();
    //    private ICCardManager cardManager;4
    private Economy econ;
    public static MetroCard plugin;

    @Override
    public void onEnable() {
        plugin = this;

        PaperCommandManager cmdManager = new PaperCommandManager(this);

        logger.info("Looking for Vault...");
        if (!setupEconomy() ) {
            logger.severe("Disabled due to no Vault dependency found!");
            logger.severe("Download vault at: https://www.spigotmc.org/resources/vault.34315/");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }



        logger.info("Loading config..");
        this.saveDefaultConfig();

        logger.info("Loading commands...");
        cmdManager.registerCommand(new Commands());

        cmdManager.enableUnstableAPI("help");


        logger.info("Loading data....");
        System.out.println("data loaded: "+createDataFile());

        logger.info("Registering listeners....");
        regEvents();

        logger.info("MetroCard " + this.getDescription().getVersion() + " has been enabled :D");

        logger.info("Starting data saver");
        startSaveRunner();


    }

    @Override
    public void onDisable() {
        logger.info(this::getRandomMessage);
        logger.info("MetroCard " + this.getDescription().getVersion() + " has been disabled");


    }

    private String getRandomMessage() {
        String[] msgs = {"> Have a great weekend ~!", "> Bye brian ~~", "> See ya later ~!", "> Terminated, at the station."};
        int randindex = new Random().nextInt(3);
        return msgs[randindex];
    }

    private boolean createDataFile() {
        dataFile = new File(getDataFolder(), "data.yml");
        if (!dataFile.exists()) {
            dataFile.getParentFile().mkdirs();
            saveResource("data.yml", false);
        }
        this.data = new YamlConfiguration();
        try {
            data.load(dataFile);
            return true;
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            return false;
        }

    }

    public FileConfiguration getData() {
        return data;
    }

    public Economy getEcon() {
        return econ;
    }

    public void saveData() {
        try {
            this.data.save(dataFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startSaveRunner() {
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            saveData();
            logger.info("Data saved");
        }, 0, this.getConfig().getLong("save-interval") * 20);
    }

    private void regEvents(){
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(new SignClickChangeListener(),this);
        pm.registerEvents(new ICTouchListener(),this);

    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
}

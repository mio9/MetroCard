package sh.mio9.spigot.metrocard;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;
import sh.mio9.spigot.metrocard.util.ColorParser;

import java.util.Set;

public class ICCardManager {


    public static ICCard addCard() {
        ICCard newCard = new ICCard();
        MetroCard.plugin.getData().set("cards." + newCard.getCardID().toUpperCase() + ".balance", newCard.getBalance());
        MetroCard.plugin.getData().set("cards." + newCard.getCardID().toUpperCase() + ".active", newCard.isActive());
        return newCard;
    }

    public static ICCard addCard(double balance) {
        ICCard newCard = new ICCard(balance);
        MetroCard.plugin.getData().set("cards." + newCard.getCardID().toUpperCase() + ".balance", newCard.getBalance());
        MetroCard.plugin.getData().set("cards." + newCard.getCardID().toUpperCase() + ".active", newCard.isActive());
        return newCard;
    }

    @Nullable
    public static ICCard forgeCard(String ID, double balance) {
        if (MetroCard.plugin.getData().contains("cards." + ID.toUpperCase())) {
            return null;
            // if the card already exists, quit
        } else {
            // no overlapping
            ICCard newCard = new ICCard(ID, balance, true);
            MetroCard.plugin.getData().set("cards." + newCard.getCardID().toUpperCase() + ".balance", newCard.getBalance());
            MetroCard.plugin.getData().set("cards." + newCard.getCardID().toUpperCase() + ".active", newCard.isActive());
            return newCard;
        }

    }

    public static void deactivateCard(String cardId) {
        MetroCard.plugin.getData().set("cards." + cardId.toUpperCase() + ".active", false);
    }

    public static void activateCard(String cardId) {
        MetroCard.plugin.getData().set("cards." + cardId.toUpperCase() + ".active", true);
    }

    /**
     * @param cardId the card id to search the card
     * @return the ICCard object if the card exists else NULL
     */
    @Nullable
    public static ICCard getCard(String cardId) {
        if (MetroCard.plugin.getData().contains("cards." + cardId.toUpperCase())) {
            double balance = MetroCard.plugin.getData().getDouble("cards." + cardId.toUpperCase() + ".balance");
            boolean active = MetroCard.plugin.getData().getBoolean("cards." + cardId.toUpperCase() + ".active");
            return new ICCard(cardId, balance, active);
        } else {
            return null;
        }

    }

    /**
     * @param item "Card Item" to get card object
     * @return ICCard object
     */
    @Nullable
    public static ICCard getCard(ItemStack item) {
        if (!item.hasItemMeta()) {
            return null;
        }
        if (item.getItemMeta().getDisplayName().equals(ColorParser.p(MetroCard.plugin.getConfig().getString("card-name"))) && item.getType().equals(Material.getMaterial(MetroCard.plugin.getConfig().getString("card-item")))) {
            String onCardID = ColorParser.removeColor(item.getItemMeta().getLore().get(1));
            ICCard card = ICCardManager.getCard(onCardID);
            // check if it's an exist card (not a force made card)
            if (card != null) {
                return card;
            } else {
                // it's a card, but not valid
                return null;
            }
        } else {
            // it's not card item
            return null;
        }
    }

    /**
     * Purge all inactivated cards from the card registry
     */
    public static void purge() {
        Set<String> cardIDs = MetroCard.plugin.getData().getConfigurationSection("cards").getKeys(false);

        cardIDs.forEach(id -> {
            boolean active = MetroCard.plugin.getData().getBoolean("cards." + id + ".active");
            if (!active) {
                // so something
            }
        });
        // future feature
    }

    public static void updateCardData(ICCard card) {
        MetroCard.plugin.getData().set("cards." + card.getCardID().toUpperCase() + ".balance", card.getBalance());
        MetroCard.plugin.getData().set("cards." + card.getCardID().toUpperCase() + ".active", card.isActive());
    }


}

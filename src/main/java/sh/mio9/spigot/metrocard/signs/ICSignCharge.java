package sh.mio9.spigot.metrocard.signs;

import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

public class ICSignCharge extends ICSign implements ConfigurationSerializable {

    public ICSignCharge(Location location) {
        super(location);
    }

    public ICSignCharge(Map<String, Object> map) {
        super((Location) map.get("loc"));
        this.signActions = (LinkedList<String>) map.get("actions");
    }

    public Map<String, Object> serialize() {
        Map map = new TreeMap();
        map.put("loc", location);
        map.put("actions",signActions);
        return map;
    }
}

package sh.mio9.spigot.metrocard.signs;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.Gate;
import org.bukkit.material.Lever;
import sh.mio9.spigot.metrocard.MetroCard;

import java.util.LinkedList;

public abstract class ICSign {
    Location location;
    LinkedList<String> signActions;

    public ICSign(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public LinkedList<String> getSignActions() {
        return signActions;
    }

    public void removeSignAction(int index) {
        signActions.remove(index);
    }

    public void addSignAction(String actionString) {
        signActions.add(actionString);
    }

    public void runActions(Player player) {
        signActions.forEach(actionString -> {
            if (actionString.startsWith("power:")) {
                // power a lever / open a gate
                // e.g.
                // power:60,50,42
                String[] coordString = actionString.replace("power:", "").split(",");
                Block block = player.getWorld().getBlockAt(Integer.parseInt(coordString[0]), Integer.parseInt(coordString[1]), Integer.parseInt(coordString[2]));
                //todo here

                if (block.getState() instanceof Gate){
                    ((Gate) block.getState()).setOpen(true);
                }
            } else if (actionString.startsWith("console:")) {
                // run command on behalf of server
                // placeholder %player% -> playername of the player touched the sign
                String command = actionString.replaceFirst("console:", "");
                MetroCard.plugin.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replace("%player%", player.getName()));
            } else if (actionString.startsWith("playercmd:")) {
                // run on behalf of the player
                String command = actionString.replaceFirst("playercmd:", "");
                player.performCommand(command.replace("%player%", player.getName()));
            }
        });
    }
}

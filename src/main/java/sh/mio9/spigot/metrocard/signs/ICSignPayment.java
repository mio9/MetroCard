package sh.mio9.spigot.metrocard.signs;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class ICSignPayment extends ICSign implements ConfigurationSerializable {
    double amount;
    Player receiver;


    public ICSignPayment(Location location, double amount, Player receiver) {
        super(location);
        this.amount = amount;
        this.receiver = receiver;
    }

    public double getAmount() {
        return amount;
    }

    public Player getReceiver() {
        return receiver;
    }

    public ICSignPayment(Map<String, Object> map) {
        super((Location) map.get("loc"));
        this.amount = (double) map.get("amount");
        this.receiver = (Player) Bukkit.getPlayer((UUID) UUID.fromString((String) map.get("receiver")));
    }

    @Override
    public Map<String, Object> serialize() {
        Map map = new TreeMap();
        map.put("loc", location);
        map.put("amount", amount);
        map.put("receiver", receiver.getUniqueId().toString());
        return map;
    }
}

package sh.mio9.spigot.metrocard.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import co.aikar.commands.contexts.OnlinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import sh.mio9.spigot.metrocard.ICCard;
import sh.mio9.spigot.metrocard.ICCardManager;
import sh.mio9.spigot.metrocard.ICSignType;
import sh.mio9.spigot.metrocard.MetroCard;
import sh.mio9.spigot.metrocard.util.ColorParser;


@CommandAlias("metrocard|mcard|ic")
public class Commands extends BaseCommand {
//    final String CARD_INIT_BALANCE

    @Default
    @Subcommand("help|list")
    @Description("Help menu of the plugin")
    @CommandPermission("metrocard.command.help")
    public void onHelp(CommandSender sender, CommandHelp help) {
        sender.sendMessage("=========[MetroCard]=========");
        help.showHelp();
    }

    @Subcommand("balance|bal")
    @CommandPermission("metrocard.command.balance")
    public void onCheckBal(CommandSender sender, @Optional String cardID) {
        if (!(sender instanceof Player) && cardID == null) {
            sender.sendMessage(ColorParser.p("&cCannot detect card-on-hand for non-player, Please specify cardID"));
            return;
        }
        if (cardID == null) {
            // card id not specified
            Player player = (Player) sender;
            ItemStack cardOnHand = player.getInventory().getItemInMainHand();

            // check if its card item
//            if (!cardOnHand.hasItemMeta()) {
//                player.sendMessage(ColorParser.p("&cYou are not holding a card item"));
//                return;
//            }
//            if (cardOnHand.getItemMeta().getDisplayName().equals(ColorParser.p(MetroCard.plugin.getConfig().getString("card-name"))) && cardOnHand.getType().equals(Material.getMaterial(MetroCard.plugin.getConfig().getString("card-item")))) {
//                String onCardID = ColorParser.removeColor(cardOnHand.getItemMeta().getLore().get(1));
//                ICCard card = ICCardManager.getCard(onCardID);
//                // check if it's an exist card (not a force made card)
//                if (card != null) {
//                    sender.sendMessage(ColorParser.p("&aCard balance of " + card.getCardID() + ": &e" + card.getBalance()));
//
//
//                } else {
//                    // it's a card, but not valid
//                    player.sendMessage(ColorParser.p("&cThe card you holding is not a valid/existing card"));
//                }
//            } else {
//                // it's not card item
//                player.sendMessage(ColorParser.p("&cYou are not holding a card item"));
//            }
            ICCard card = ICCardManager.getCard(cardOnHand);
            if (card != null) {

                player.sendMessage(ColorParser.p("&aCard balance of " + card.getCardID() + ": &e" + String.format("%.2f",card.getBalance())));
            } else {
                player.sendMessage(ColorParser.p("&cThe item you holding is not a valid/existing card"));
            }
        } else {
            // the card ID is specified
            ICCard card = ICCardManager.getCard(cardID);
            // check if it's an exist card (not a force made card)
            if (card != null) {
                sender.sendMessage(ColorParser.p("&aCard balance of " + card.getCardID() + ": &e" + String.format("%.2f",card.getBalance())));
            } else {
                // it's a card, but not valid
                sender.sendMessage(ColorParser.p("&cThe card you specified is not a valid/existing card"));
            }
        }

    }


    @Subcommand("reload")
    @Description("Reload plugin config")
    @CommandPermission("metrocard.admin.reload")
    public void onReload(CommandSender sender) {
        MetroCard.plugin.reloadConfig();
        sender.sendMessage(ColorParser.p("&aConfig reloaded"));
        MetroCard.plugin.getLogger().info("Configuration Reloaded");
    }

    @Subcommand("create")
    @Syntax("[init_balance] [player]")
    @CommandCompletion("@players")
    @Description("Create a new IC Card (with initial balance and target player)")
    @CommandPermission("metrocard.command.create")
    public void onCreateCard(CommandSender sender, @Default("default") String initBalance, @Optional OnlinePlayer player) {
        if (player != null) {
            //debug

//            sender.sendMessage(initBalance + "||" + player.player.getInventory().getItemInMainHand());

            //Create card for others
            //prod
            try {
                double balance = Double.parseDouble(initBalance);
                ICCard card = ICCardManager.addCard(balance);
                player.getPlayer().getInventory().addItem(card.getItem());
                sender.sendMessage(ColorParser.p("&aCreated new card with balance: &e" + initBalance + " &ato &e" + player.getPlayer().getName()));
            } catch (NumberFormatException e) {
                ICCard card = ICCardManager.addCard();
                player.getPlayer().getInventory().addItem(card.getItem());
                sender.sendMessage(ColorParser.p("&aCreated new card with &edefault init balance" + " &ato &e" + player.getPlayer().getName()));
            }


        } else {
            //debug
//            sender.sendMessage(initBalance + "||" + sender.getPlayer().getInventory().getItemInMainHand());
            if (!(sender instanceof Player)) {
                sender.sendMessage(ColorParser.p("&cNon player cannot create card for your own"));
                return;
            }
            Player playerSender = (Player) sender;
            // Create card for yourself
            try {
                double balance = Double.parseDouble(initBalance);
                ICCard card = ICCardManager.addCard(balance);
                playerSender.getInventory().addItem(card.getItem());
                playerSender.sendMessage(ColorParser.p("&aCreated new card with balance: &e" + initBalance + " &afor " + sender.getName()));
            } catch (NumberFormatException e) {
                ICCard card = ICCardManager.addCard();
                playerSender.getInventory().addItem(card.getItem());
                playerSender.sendMessage(ColorParser.p("&aCreated new card with &edefault init balance &afor &e" + sender.getName()));
            }
        }

    }

    @Subcommand("changebalance|changebal|cb|change")
    @Description("Alter balance of the card")
    @Syntax("<amount> [cardID]")
    @CommandPermission("metrocard.command.changebalance")
    public void onChangeBalance(CommandSender sender, double amount, @Optional String cardID) {
        if (cardID == null && !(sender instanceof Player)) {
            sender.sendMessage(ColorParser.p("&cCannot detect card-on-hand for non-player, Please specify cardID"));
            return;
        }
        if (cardID == null) {
            // card id not specified
            Player player = (Player) sender;
            ItemStack cardOnHand = player.getInventory().getItemInMainHand();

//            // check if its card item
//            if (!cardOnHand.hasItemMeta()) {
//                player.sendMessage(ColorParser.p("&cYou are not holding a card item"));
//                return;
//            }
//            if (cardOnHand.getItemMeta().getDisplayName().equals(ColorParser.p(MetroCard.plugin.getConfig().getString("card-name"))) && cardOnHand.getType().equals(Material.getMaterial(MetroCard.plugin.getConfig().getString("card-item")))) {
//                String onCardID = ColorParser.removeColor(cardOnHand.getItemMeta().getLore().get(1));
//                ICCard card = ICCardManager.getCard(onCardID);
//                // check if it's an exist card (not a force made card)
//                if (card != null) {
////                    System.out.println(card.getBalance());
//                    card.setBalance(card.getBalance() + amount);
//                    player.sendMessage(ColorParser.p("&aAltered balance of %id% by %amount%")
//                            .replace("%id%", card.getCardID())
//                            .replace("%amount%", String.valueOf(amount)));
//
//                } else {
//                    // it's a card, but not valid
//                    player.sendMessage(ColorParser.p("&cThe card you holding is not a valid/existing card"));
//                }
//            } else {
//                // it's not card item
//                player.sendMessage(ColorParser.p("&cYou are not holding a card item"));
            ICCard card = ICCardManager.getCard(cardOnHand);
            if (card != null) {
                card.setBalance(card.getBalance() + amount);
                player.sendMessage(ColorParser.p("&aAltered balance of %id% by %amount%")
                        .replace("%id%", card.getCardID())
                        .replace("%amount%", String.valueOf(amount)));
            } else {
                player.sendMessage(ColorParser.p("&cThe item you holding is not a valid/existing card"));
            }
//            }
        } else {
            // the card ID is specified
            ICCard card = ICCardManager.getCard(cardID);
            // check if it's an exist card (not a force made card)
            if (card != null) {
//                System.out.println(card.getBalance());
                card.setBalance(card.getBalance() + amount);
                sender.sendMessage(ColorParser.p("&aAltered balance of %id% by %amount%")
                        .replace("%id%", card.getCardID())
                        .replace("%amount%", String.valueOf(amount)));

            } else {
                // it's a card, but not valid
                sender.sendMessage(ColorParser.p("&cThe card you specified is not a valid/existing card"));
            }
        }
    }

    @Subcommand("listcard|listcards")
    @Description("List current active cards")
    @CommandPermission("metrocard.command.listcard")
    public void onListCard(CommandSender sender) {
        sender.sendMessage("[listcard] Future function");
    }

    @Subcommand("purge|removeinactive")
    @Description("List current active cards")
    @CommandPermission("metrocard.command.purge")
    public void onPurge(CommandSender sender) {
        sender.sendMessage("[purge] Future function");
    }

    @Subcommand("forge")
    @Description("Forcibly forge a card with balance and a custom card ID")
    @CommandPermission("metrocard.special.forge")
    public void onForge(Player player, String id, double balance) {
        ICCard newCard = ICCardManager.forgeCard(id, balance);
        if (newCard == null) {
            player.sendMessage(ColorParser.p("&cThe ID is already taken by an existing card"));
            return;
        }
        ItemStack cardItem = newCard.getItem();
        player.getInventory().addItem(cardItem);

    }

    @Subcommand("get")
    @Description("Get a card by its ID")
    @CommandPermission("metrocard.command.get")
    public void onGet(Player player, String id) {
        ICCard newCard = ICCardManager.getCard(id);
        if (newCard != null) {
            ItemStack cardItem = newCard.getItem();
            player.getInventory().addItem(cardItem);
            player.sendMessage(ColorParser.p("&aGiven card &e" + newCard.getCardID()));
        } else {
            player.sendMessage(ColorParser.p("&cThe given card ID is not valid"));
        }

    }

    @Subcommand("editsign|es")
    @Description("Edit (price) information of a sign")
    @CommandPermission("metrocard.command.editsign")
    public void onEditSign(Player player,double newAmount) {
        Block block = player.getTargetBlock(null, 5);
        if (block.getState() instanceof Sign) {
            Sign sign = (Sign) block.getState();
            // removed the stripcolor on purpose to avoid forged signs
            ICSignType type = ICSignType.get(sign.getLine(0));
            if (type == null) {
                return;
            }
            sign.setLine(1, String.valueOf(newAmount));
            sign.update();
        }

    }

    @Subcommand("activate")
    @Description("Activate a card")
    @CommandPermission("metrocard.command.activate")
    public void onActivateCard(Player player,String id) {
        ICCard card = ICCardManager.getCard(id);
        if (card!=null){
            card.setActive(true);
            player.sendMessage(ColorParser.p("&aActivated card: "+id));
        } else {
            player.sendMessage(ColorParser.p("&cThe given card ID is not valid"));
        }

    }

    @Subcommand("deactivate")
    @Description("Deactivate a card")
    @CommandPermission("metrocard.command.deactivate")
    public void onDeactivateCard(Player player,String id) {
        ICCard card = ICCardManager.getCard(id);
        if (card!=null){
            card.setActive(false);
            player.sendMessage(ColorParser.p("&aDeactivated card: "+id));
        } else {
            player.sendMessage(ColorParser.p("&cThe given card ID is not valid"));
        }

    }
}

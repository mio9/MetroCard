package sh.mio9.spigot.metrocard.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Subcommand;
import org.bukkit.entity.Player;


@CommandAlias("metrocard|mcard|ic")
public class ActionCommands extends BaseCommand {

    @Subcommand("action list")
    @CommandPermission("metrocard.command.action.list")
    public void onActionList(Player player){

    }

    @Subcommand("action remove")
    @CommandPermission("metrocard.command.action.list")
    public void onActionRemove(Player player, int index){

    }

    @Subcommand("action add")
    @CommandPermission("metrocard.command.action.list")
    public void onActionAdd(Player player){

    }

    @Subcommand("action insert")
    @CommandPermission("metrocard.command.action.list")
    public void onActionList(Player player, int index){

    }


}

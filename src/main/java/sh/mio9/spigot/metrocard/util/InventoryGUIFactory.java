package sh.mio9.spigot.metrocard.util;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryGUIFactory {
    private Inventory inv;

    public InventoryGUIFactory(int size,String title) {
        this.inv = Bukkit.createInventory(null,size,title);
    }

    public InventoryGUIFactory fill(int topLeft,int bottomRight){
        return this;
    }

    public InventoryGUIFactory fillAll(ItemStack item){
        return this;
    }

    public InventoryGUIFactory set(int slot, ItemStack item){
        return this;
    }

    public Inventory build(){
        return this.inv;
    }
}

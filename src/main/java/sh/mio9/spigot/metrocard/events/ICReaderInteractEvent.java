package sh.mio9.spigot.metrocard.events;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import sh.mio9.spigot.metrocard.ICCard;
import sh.mio9.spigot.metrocard.ICSignType;

public class ICReaderInteractEvent extends Event {

    private final ICCard card;
    private final ICSignType signType;
    private final Sign sign;
    private final Player player;

    public ICReaderInteractEvent(ICCard card, ICSignType signType, Sign sign, Player player) {
        this.card = card;
        this.signType = signType;
        this.sign = sign;
        this.player = player;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    public ICCard getCard() {
        return card;
    }

    public ICSignType getSignType() {
        return signType;
    }

    public Sign getSign() {
        return sign;
    }

    public Player getPlayer() {
        return player;
    }
}

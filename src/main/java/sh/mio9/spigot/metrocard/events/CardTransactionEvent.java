package sh.mio9.spigot.metrocard.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.Contract;
import sh.mio9.spigot.metrocard.ICCard;
import sh.mio9.spigot.metrocard.PaymentAccount;


public class CardTransactionEvent  extends Event{
    private static final HandlerList handlerList= new HandlerList();


    /**
     * The IC Card involved in the transaction
     */
    private ICCard icCard;
    /**
     * Amount reduced FROM the card
     */
    private double amountReduced;
    /**
     * Amount added TO the card
     */
    private double amountAdded;
    /**
     * The other side of transaction (e.g. the charging machine / a payment receiving account)
     */
    private PaymentAccount paymentAccount;

    public CardTransactionEvent(ICCard icCard, double amountReduced, double amountAdded, PaymentAccount paymentAccount) {
        this.icCard = icCard;
        this.amountReduced = amountReduced;
        this.amountAdded = amountAdded;
        this.paymentAccount = paymentAccount;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }



    public ICCard getIcCard() {
        return icCard;
    }

    public double getAmountReduced() {
        return amountReduced;
    }

    public double getAmountAdded() {
        return amountAdded;
    }

    public PaymentAccount getPaymentAccount() {
        return paymentAccount;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
